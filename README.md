# Linux Game

![Build Status](https://gitlab.com/jjv.dangelo/game-linux/badges/master/pipeline.svg)

This is a little playground to play around with game development--from scratch--
in Linux. I don't know where this is gonna go, but it's [MIT licensed](LICENSE)
if you're interested in doing something with what I have here.
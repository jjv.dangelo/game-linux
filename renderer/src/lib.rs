use math::{Mat4, Vec2, Vec3};

#[derive(Clone, Copy)]
pub struct Vertex {
    pub pos: Vec3,
    pub tex: Vec2,
}

impl Vertex {
    #[inline]
    pub const fn new(pos: Vec3, tex: Vec2) -> Self {
        Self { pos, tex }
    }
}

pub enum RenderCommand<M: Sized> {
    ClearScreen(Vec3),
    SetVPMat(Mat4),
    SetModelBasis(Mat4),
    DrawMesh(M),
}

pub trait Renderer<M: Sized> {
    type Mesh;
    type Texture;

    fn update_viewport(&mut self, width: i32, height: i32);
    fn frame<F: FnMut(&mut Vec<RenderCommand<M>>)>(&mut self, f: F);
    fn load_mesh(&mut self, verts: &[Vertex], tex: Option<Self::Texture>) -> Self::Mesh;
    fn load_tex<P: AsRef<std::path::Path>>(&mut self, p: P) -> Option<Self::Texture>;
}

pub trait CommandList<M: Sized> {
    fn push_clearscreen(&mut self, color: Vec3);
    fn push_vp_matrix(&mut self, vp: Mat4);
    fn push_model_basis(&mut self, b: Mat4);
    fn push_draw_mesh(&mut self, mesh: M);
}

impl<M: Sized> CommandList<M> for Vec<RenderCommand<M>> {
    #[inline]
    fn push_clearscreen(&mut self, color: Vec3) {
        self.push(RenderCommand::ClearScreen(color));
    }

    #[inline]
    fn push_vp_matrix(&mut self, vp: Mat4) {
        self.push(RenderCommand::SetVPMat(vp));
    }

    #[inline]
    fn push_model_basis(&mut self, b: Mat4) {
        self.push(RenderCommand::SetModelBasis(b));
    }

    #[inline]
    fn push_draw_mesh(&mut self, mesh: M) {
        self.push(RenderCommand::DrawMesh(mesh));
    }
}

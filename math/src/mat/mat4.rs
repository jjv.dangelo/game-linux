use crate::{CrossProduct, DotProduct, Vec3, Vec4};
use std::ops;

/// A 4x4 matrix structure.
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Mat4 {
    data: [f32; 4 * 4],
}

impl Mat4 {
    /// Creates an identity matrix.
    ///
    /// ```
    ///# use math::Mat4;
    /// let ident = Mat4::IDENT;
    /// assert_eq!(ident, [
    ///     1., 0., 0., 0.,
    ///     0., 1., 0., 0.,
    ///     0., 0., 1., 0.,
    ///     0., 0., 0., 1.,
    /// ].into());
    /// ```
    pub const IDENT: Self = Self {
        data: [
            1., 0., 0., 0.,
            0., 1., 0., 0.,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ],
    };

    /// Creates a matrix with the specified x-offset.
    #[inline]
    pub const fn x_offset(x: f32) -> Self {
        Self { data: [
            1., 0., 0., x,
            0., 1., 0., 0.,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ] }
    }

    /// Creates a matrix with the specified y-offset.
    #[inline]
    pub const fn y_offset(y: f32) -> Self {
        Self { data: [
            1., 0., 0., 0.,
            0., 1., 0., y,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ] }
    }

    /// Creates a matrix with the specified z-offset.
    #[inline]
    pub const fn z_offset(z: f32) -> Self {
        Self { data: [
            1., 0., 0., 0.,
            0., 1., 0., 0.,
            0., 0., 1., z,
            0., 0., 0., 1.,
        ] }
    }

    /// Creates a matrix with the specified x-, y-, and z-offsets.
    #[inline]
    pub const fn offset(x: f32, y: f32, z: f32) -> Self {
        Self { data: [
            1., 0., 0., x,
            0., 1., 0., y,
            0., 0., 1., z,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub fn x_rotation(radians: f32) -> Self {
        Self { data: [
            1., 0., 0., 0.,
            0., radians.cos(), -(radians.sin()), 0.,
            0., radians.sin(), radians.cos(), 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub fn y_rotation(radians: f32) -> Self {
        Self { data: [
            radians.cos(), 0., radians.sin(), 0.,
            0., 1., 0., 0.,
            -(radians.sin()), 0., radians.cos(), 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub fn z_rotation(radians: f32) -> Self {
        Self { data: [
            radians.cos(), -(radians.sin()), 0., 0.,
            radians.sin(), radians.cos(), 0., 0.,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn x_scale(scalar: f32) -> Self {
        Self { data: [
            scalar, 0., 0., 0.,
            0., 1., 0., 0.,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn y_scale(scalar: f32) -> Self {
        Self { data: [
            1., 0., 0., 0.,
            0., scalar, 0., 0.,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn z_scale(scalar: f32) -> Self {
        Self { data: [
            1., 0., 0., 0.,
            0., 1., 0., 0.,
            0., 0., scalar, 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn scale(Vec3 { x, y, z }: Vec3) -> Self {
        Self { data: [
            x, 0., 0., 0.,
            0., y, 0., 0.,
            0., 0., z, 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn scale_unif(scalar: f32) -> Self {
        Self { data: [
            scalar, 0., 0., 0.,
            0., scalar, 0., 0.,
            0., 0., scalar, 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn trans(self) -> Self {
        let m = self.data;

        // 0  1  2  3
        // 4  5  6  7
        // 8  9  10 11
        // 12 13 14 15
        Self { data: [
            m[0], m[4], m[8], m[12],
            m[1], m[5], m[9], m[13],
            m[2], m[6], m[10], m[14],
            m[3], m[7], m[11], m[15],
        ] }
    }

    /// Calculates the determinant of the matrix.
    ///
    /// ```
    ///# use math::Mat4;
    /// let m: Mat4 = [
    ///     4., 3., 2., 2.,
    ///     0., 1., -3., 3.,
    ///     0., -1., 3., 3.,
    ///     0., 3., 1., 1.,
    /// ].into();
    /// assert_eq!(m.det(), -240.);
    /// ```
    #[inline]
    pub fn det(&self) -> f32 {
        let m = &self.data;

         m[0] * Vec3::new(m[5], m[6], m[7]).cross(Vec3::new(m[9], m[10], m[11])).dot(Vec3::new(m[13], m[14], m[15]))
        -m[1] * Vec3::new(m[4], m[6], m[7]).cross(Vec3::new(m[8], m[10], m[11])).dot(Vec3::new(m[12], m[14], m[15]))
        +m[2] * Vec3::new(m[4], m[5], m[7]).cross(Vec3::new(m[8], m[9], m[11])).dot(Vec3::new(m[12], m[13], m[15]))
        -m[3] * Vec3::new(m[4], m[5], m[6]).cross(Vec3::new(m[8], m[9], m[10])).dot(Vec3::new(m[12], m[13], m[14]))
    }

    #[inline]
    pub fn adj(&self) -> Self {
        let m = &self.data;

        Self { data: [
            Vec3::new(m[5], m[6], m[7]).cross(Vec3::new(m[9], m[10], m[11])).dot(Vec3::new(m[13], m[14], m[15])),
           -Vec3::new(m[4], m[6], m[7]).cross(Vec3::new(m[8], m[10], m[11])).dot(Vec3::new(m[12], m[14], m[15])),
            Vec3::new(m[4], m[5], m[7]).cross(Vec3::new(m[8], m[9], m[11])).dot(Vec3::new(m[12], m[13], m[15])),
           -Vec3::new(m[4], m[5], m[6]).cross(Vec3::new(m[8], m[9], m[10])).dot(Vec3::new(m[12], m[13], m[14])),

            Vec3::new(m[1], m[2], m[3]).cross(Vec3::new(m[9], m[10], m[11])).dot(Vec3::new(m[13], m[14], m[15])),
           -Vec3::new(m[0], m[2], m[3]).cross(Vec3::new(m[8], m[10], m[11])).dot(Vec3::new(m[12], m[14], m[15])),
            Vec3::new(m[0], m[1], m[3]).cross(Vec3::new(m[8], m[9], m[11])).dot(Vec3::new(m[12], m[13], m[15])),
           -Vec3::new(m[0], m[1], m[2]).cross(Vec3::new(m[8], m[9], m[10])).dot(Vec3::new(m[12], m[13], m[14])),

            Vec3::new(m[1], m[2], m[3]).cross(Vec3::new(m[5], m[6], m[7])).dot(Vec3::new(m[13], m[14], m[15])),
           -Vec3::new(m[0], m[2], m[3]).cross(Vec3::new(m[4], m[6], m[7])).dot(Vec3::new(m[12], m[14], m[15])),
            Vec3::new(m[0], m[1], m[3]).cross(Vec3::new(m[4], m[5], m[7])).dot(Vec3::new(m[12], m[13], m[15])),
           -Vec3::new(m[0], m[1], m[2]).cross(Vec3::new(m[4], m[5], m[6])).dot(Vec3::new(m[12], m[13], m[14])),

            Vec3::new(m[1], m[2], m[3]).cross(Vec3::new(m[5], m[6], m[7])).dot(Vec3::new(m[9], m[10], m[11])),
           -Vec3::new(m[0], m[2], m[3]).cross(Vec3::new(m[4], m[6], m[7])).dot(Vec3::new(m[8], m[10], m[11])),
            Vec3::new(m[0], m[1], m[3]).cross(Vec3::new(m[4], m[5], m[7])).dot(Vec3::new(m[8], m[9], m[11])),
           -Vec3::new(m[0], m[1], m[2]).cross(Vec3::new(m[4], m[5], m[6])).dot(Vec3::new(m[8], m[9], m[10])),
        ] }.trans() * (1. / self.det()) // TODO: Implement ops::Div<f32> for Mat4
    }
}

impl ops::Mul for Mat4 {
    type Output = Mat4;

    /// Multiplies two `Mat4`.
    ///
    /// ```
    ///# use math::Mat4;
    /// let m1 = Mat4::IDENT;
    /// let m2 = Mat4::IDENT;
    /// assert_eq!(m1 * m2, Mat4::IDENT);
    /// ```
    #[inline]
    fn mul(self, Mat4 { data: d2 }: Self) -> Self::Output {
        let Mat4 { data: d1 } = self;

        Self { data: [
            d1[0] * d2[0] + d1[1] * d2[4] + d1[2] * d2[8] + d1[3] * d2[12],
            d1[0] * d2[1] + d1[1] * d2[5] + d1[2] * d2[9] + d1[3] * d2[13],
            d1[0] * d2[2] + d1[1] * d2[6] + d1[2] * d2[10] + d1[3] * d2[14],
            d1[0] * d2[3] + d1[1] * d2[7] + d1[2] * d2[11] + d1[3] * d2[15],

            d1[4] * d2[0] + d1[5] * d2[4] + d1[6] * d2[8] + d1[7] * d2[12],
            d1[4] * d2[1] + d1[5] * d2[5] + d1[6] * d2[9] + d1[7] * d2[13],
            d1[4] * d2[2] + d1[5] * d2[6] + d1[6] * d2[10] + d1[7] * d2[14],
            d1[4] * d2[3] + d1[5] * d2[7] + d1[6] * d2[11] + d1[7] * d2[15],

            d1[8] * d2[0] + d1[9] * d2[4] + d1[10] * d2[8] + d1[11] * d2[12],
            d1[8] * d2[1] + d1[9] * d2[5] + d1[10] * d2[9] + d1[11] * d2[13],
            d1[8] * d2[2] + d1[9] * d2[6] + d1[10] * d2[10] + d1[11] * d2[14],
            d1[8] * d2[3] + d1[9] * d2[7] + d1[10] * d2[11] + d1[11] * d2[15],

            d1[12] * d2[0] + d1[13] * d2[4] + d1[14] * d2[8] + d1[15] * d2[12],
            d1[12] * d2[1] + d1[13] * d2[5] + d1[14] * d2[9] + d1[15] * d2[13],
            d1[12] * d2[2] + d1[13] * d2[6] + d1[14] * d2[10] + d1[15] * d2[14],
            d1[12] * d2[3] + d1[13] * d2[7] + d1[14] * d2[11] + d1[15] * d2[15],
        ] }
    }
}

impl ops::Mul<f32> for Mat4 {
    type Output = Self;

    #[inline]
    fn mul(mut self, s: f32) -> Self {
        for i in self.data.iter_mut() {
            *i *= s;
        }

        self
    }
}

impl<T> ops::Mul<T> for Mat4
where
    T: Into<Vec4>,
{
    type Output = Vec4;

    /// Multiplies `Mat4` times a `Vec4`.
    ///
    /// ```
    ///# use math::{Mat4, Vec4};
    /// let m = Mat4::IDENT;
    /// let v = Vec4::new(3., 4., 5., 6.);
    /// assert_eq!(m * v, v);
    ///
    /// let n = [3., 4., 5., 6.];
    /// assert_eq!(m * n, v);
    ///
    /// let n = (3., 4., 5., 6.);
    /// assert_eq!(m * n, v);
    /// ```
    #[inline]
    fn mul(self, other: T) -> Self::Output {
        let other = other.into();
        let Mat4 { data: d } = self;

        Vec4::new(
            d[00] * other.x + d[01] * other.y + d[02] * other.z + d[03] * other.w,
            d[04] * other.x + d[05] * other.y + d[06] * other.z + d[07] * other.w,
            d[08] * other.x + d[09] * other.y + d[10] * other.z + d[11] * other.w,
            d[12] * other.x + d[13] * other.y + d[14] * other.z + d[15] * other.w,
        )
    }
}

use std::fmt;
impl fmt::Debug for Mat4 {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[0], self.data[1], self.data[2], self.data[3]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[4], self.data[5], self.data[6], self.data[7]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[8], self.data[9], self.data[10], self.data[11]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[12], self.data[13], self.data[14], self.data[15]
        )
    }
}

impl From<[f32; 4 * 4]> for Mat4 {
    #[inline]
    fn from(data: [f32; 4 * 4]) -> Self {
        Self { data }
    }
}

use crate::{CrossProduct, DotProduct, Vec3};
use std::ops;

/// A 3x3 matrix structure.
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Mat3 {
    data: [f32; 3 * 3],
}

impl Mat3 {
    /// Creates an identity matrix.
    ///
    /// ```
    ///# use math::Mat3;
    /// let ident = Mat3::IDENT;
    /// assert_eq!(ident, [
    ///     1., 0., 0.,
    ///     0., 1., 0.,
    ///     0., 0., 1.,
    /// ].into());
    /// ```
    pub const IDENT: Self = Self {
        data: [1., 0., 0., 0., 1., 0., 0., 0., 1.],
    };

    /// Transposes a matrix, converting columns to rows.
    ///
    /// ```
    ///# use math::Mat3;
    /// let mat: Mat3 = [
    ///     1., 2., 3.,
    ///     4., 5., 6.,
    ///     7., 8., 9.,
    /// ].into();
    /// let trans = mat.trans();
    /// assert_eq!(trans, [
    ///   1., 4., 7.,
    ///   2., 5., 8.,
    ///   3., 6., 9.,
    /// ].into());
    /// ```
    #[inline]
    pub fn trans(&self) -> Self {
        let Self { data: d } = self;

        Self { data: [
            d[0], d[3], d[6],
            d[1], d[4], d[7],
            d[2], d[5], d[8],
        ] }
    }

    /// Calculates the determinant of the matrix.
    ///
    /// ```
    ///# use math::Mat3;
    /// let mat: Mat3 = [
    ///     -4., -3.,  3.,
    ///      0.,  2., -2.,
    ///      1.,  4., -1.,
    /// ].into();
    /// assert_eq!(mat.det(), -24.);
    /// ```
    #[inline]
    pub fn det(&self) -> f32 {
        let m = &self.data;

        let a = Vec3::new(m[0], m[1], m[2]);
        let b = Vec3::new(m[3], m[4], m[5]);
        let c = Vec3::new(m[6], m[7], m[8]);

        a.cross(b).dot(c)
    }

    /// Calculates the adjugate of the matrix.
    ///
    /// ```
    ///# use math::Mat3;
    /// let mat: Mat3 = [
    ///     2., 0., 3.,
    ///     0., 1., 0.,
    ///     0., 0., 2.,
    /// ].into();
    /// let adj = mat.adj();
    /// assert_eq!(mat * adj, Mat3::IDENT);
    /// ```
    #[inline]
    pub fn adj(&self) -> Self {
        let m = &self.data;

        // The data are already layed out here as transposed
        // to avoid moving the data around too much.
        // We could have constructed the matrix and then
        // called `.trans()` before dividing by the determinant.
        Self { data: [
             (m[4] * m[8] - m[5] * m[7]), -(m[1] * m[8] - m[2] * m[7]),  (m[1] * m[5] - m[2] * m[4]),
            -(m[3] * m[8] - m[5] * m[6]),  (m[0] * m[8] - m[2] * m[6]), -(m[0] * m[5] - m[2] * m[3]),
             (m[3] * m[7] - m[4] * m[6]), -(m[0] * m[7] - m[1] * m[6]),  (m[0] * m[4] - m[1] * m[3]),
        ] } / self.det()
    }
}

impl ops::Mul for Mat3 {
    type Output = Self;

    /// Multiplies two `Mat3`.
    ///
    /// ```
    ///# use math::Mat3;
    /// let m1 = Mat3::IDENT;
    /// let m2 = Mat3::IDENT;
    /// assert_eq!(m1 * m2, Mat3::IDENT);
    /// ```
    #[inline]
    fn mul(self, Self { data: d2 }: Self) -> Self::Output {
        let Self { data: d1 } = self;

        Self { data: [
            d1[0] * d2[0] + d1[1] * d2[3] + d1[2] * d2[6],
            d1[0] * d2[1] + d1[1] * d2[4] + d1[2] * d2[7],
            d1[0] * d2[2] + d1[1] * d2[5] + d1[2] * d2[8],

            d1[3] * d2[0] + d1[4] * d2[3] + d1[5] * d2[6],
            d1[3] * d2[1] + d1[4] * d2[4] + d1[5] * d2[7],
            d1[3] * d2[2] + d1[4] * d2[5] + d1[5] * d2[8],

            d1[6] * d2[0] + d1[7] * d2[3] + d1[8] * d2[6],
            d1[6] * d2[1] + d1[7] * d2[4] + d1[8] * d2[7],
            d1[6] * d2[2] + d1[7] * d2[5] + d1[8] * d2[8],
        ] }
    }
}

impl ops::Div<f32> for Mat3 {
    type Output = Self;

    #[inline]
    fn div(self, d: f32) -> Self::Output {
        let m = self.data;

        let data = [
            m[0] / d, m[1] / d, m[2] / d,
            m[3] / d, m[4] / d, m[5] / d,
            m[6] / d, m[7] / d, m[8] / d,
        ];
        Self { data }
    }
}

impl ops::Mul<Vec3> for Mat3 {
    type Output = Vec3;

    /// Multiplies `Mat3` times a `Vec3`.
    ///
    /// ```
    ///# use math::{Mat3, Vec3};
    /// let m = Mat3::IDENT;
    /// let v = Vec3::new(3., 4., 5.);
    /// assert_eq!(m * v, v);
    /// ```
    #[inline]
    fn mul(self, Vec3 { x, y, z }: Vec3) -> Self::Output {
        let Mat3 { data: d } = self;

        Vec3::new(
            d[0] * x + d[1] * y + d[2] * z,
            d[3] * x + d[4] * y + d[5] * z,
            d[6] * x + d[7] * y + d[8] * z,
        )
    }
}

use std::fmt;
impl fmt::Debug for Mat3 {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "[{:?}, {:?}, {:?}]\n",
            self.data[0], self.data[1], self.data[2]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}]\n",
            self.data[3], self.data[4], self.data[5]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}]\n",
            self.data[6], self.data[7], self.data[8]
        )
    }
}

impl From<[f32; 9]> for Mat3 {
    #[inline]
    fn from(data: [f32; 9]) -> Self {
        Self { data }
    }
}

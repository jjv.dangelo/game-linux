mod vec;
pub use vec::{Vec2, Vec3, Vec4};

mod mat;
pub use mat::{Mat3, Mat4};

/// A trait for types that implement the dot or scalar product.
pub trait DotProduct {
    /// The output of the dot product.
    type Output;

    /// Calculates the dot product.
    fn dot(self, other: Self) -> Self::Output;
}

/// A trait for types that implement the cross product.
pub trait CrossProduct {
    /// The output of the cross product.
    type Output;

    /// Calculates the cross product.
    fn cross(self, other: Self) -> Self::Output;
}

pub trait Length {
    type Output;

    fn len(&self) -> Self::Output;
}

use std::{fmt, ops};

use super::super::{DotProduct, Length};

/// A two-dimensional vector using `x`- and `y`-components.
///
/// ```
///# use math::Vec2;
/// let v1 = Vec2 { x: 1.0f32, y: 1.0f32 };
///# // let _ = v1
/// ```
///
/// The vector is generic over `T`, with no specific constraints on
/// `T`. For those `T where T: std::ops::Add`
/// `Vec2` implements `std::ops::Add` and does componentwise addition.
///
/// ```
///# use math::Vec2;
/// let v1 = Vec2 { x: 1., y: 1. };
/// let v2 = Vec2 { x: 1., y: 1. };
/// let v3 = v1 + v2;
/// assert_eq!(v3.x, 2.);
/// assert_eq!(v3.y, 2.);
/// ```
///
/// Similarly, for those `T where T: std::ops::Sub`,
/// `Vec2` implements `std::ops::Sub` and does componentwise subtraction.
///
/// ```
///# use math::Vec2;
/// let v1 = Vec2 { x: 1., y: 1. };
/// let v2 = Vec2 { x: 1., y: 1. };
/// let v3 = v1 - v2;
/// assert_eq!(v3.x, 0.);
/// assert_eq!(v3.y, 0.);
/// ```
/// For `T where T: std::ops::Mul`
///
/// This allows `Vec2` to be used with floating point values as well as integers
/// without needing to use different definitions. It also allows us to multiply
/// by a scalar value.
///
/// ```
///# use math::Vec2;
/// let v = Vec2 { x: 1., y: 2. } * 2.;
/// assert_eq!(v, Vec2 { x: 2., y: 4. });
/// ```
#[derive(Clone, Copy, Default, PartialEq)]
pub struct Vec2 {
    /// The `x`-component of the vector.
    pub x: f32,
    /// The `y`-component of the vector.
    pub y: f32,
}

impl Vec2 {
    pub const ZERO: Self = Self::new(0., 0.,);
    pub const BASIS: Self = Self::new(1., 1.,);
    pub const X: Self = Self::new(1., 0.);
    pub const Y: Self = Self::new(0., 1.);

    /// A helper method that creates a new `Vec2` with
    /// the specified `x` and `y` values.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v1 = Vec2 { x: 1., y: 1. };
    /// let v2 = Vec2::new(1., 1.);
    /// assert_eq!(v1, v2);
    /// ```
    #[inline]
    pub const fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    /// Calculates the squared length of a `Vec2`.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v = Vec2::new(3., 4.);
    /// assert_eq!(v.len_sqr(), 25.);
    /// ```
    pub fn len_sqr(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }
}

impl Length for Vec2 {
    type Output = f32;

    /// Calculates the length of a `Vec2`.
    ///
    /// ```
    ///# use math::{Length, Vec2};
    /// let v = Vec2::new(3., 4.);
    /// assert_eq!(v.len(), 5.);
    /// ```
    #[inline]
    fn len(&self) -> Self::Output {
        self.len_sqr().sqrt()
    }
}

impl DotProduct for Vec2 {
    type Output = f32;

    /// Dot or scalar product between two vectors.
    ///
    /// ```
    ///# use math::{DotProduct, Vec2};
    /// let v1 = Vec2::new(2., 2.);
    /// let v2 = Vec2::new(3., 4.);
    /// assert_eq!(v1.dot(v2), 14.);
    /// ```
    #[inline]
    fn dot(self, other: Self) -> Self::Output {
        (self.x * other.x) + (self.y * other.y)
    }
}

impl ops::Neg for Vec2 {
    type Output = Vec2;

    /// Negates a `Vec2`.
    /// ```
    ///# use math::Vec2;
    /// let v = Vec2::new(1., 1.);
    /// assert_eq!(-v, Vec2::new(-1., -1.));
    /// ```
    #[inline]
    fn neg(self) -> Self::Output {
        Vec2::new(-self.x, -self.y)
    }
}

/// Add two `Vec2`s with component-wise addition.
///
/// ```
///# use math::Vec2;
/// let v1 = Vec2::new(1., 1.);
/// let v2 = Vec2::new(2., 2.);
/// assert_eq!(v1 + v2, Vec2::new(3., 3.));
/// ```
impl ops::Add for Vec2 {
    type Output = Vec2;

    #[inline]
    fn add(self, other: Self) -> Self::Output {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Sub for Vec2 {
    type Output = Vec2;

    #[inline]
    fn sub(self, other: Self) -> Self::Output {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::Mul<f32> for Vec2 {
    type Output = Vec2;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Vec2 {
            x: self.x * scalar,
            y: self.y * scalar,
        }
    }
}

impl From<[f32; 2]> for Vec2 {
    /// Creates a `Vec2` from `[f32; 2]`.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v = Vec2::from([1., 1.]);
    /// assert_eq!(v, Vec2::new(1., 1.));
    /// ```
    #[inline]
    fn from([x, y]: [f32; 2]) -> Self {
        Self::new(x, y)
    }
}

impl From<(f32, f32)> for Vec2 {
    /// Creates a `Vec2` from `(T, T)`.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v = Vec2::from((1., 1.));
    /// assert_eq!(v, Vec2::new(1., 1.));
    /// ```
    #[inline]
    fn from((x, y): (f32, f32)) -> Self {
        Self { x, y }
    }
}

impl Into<[f32; 2]> for Vec2 {
    /// Creates a `[f32; 2]` from a `Vec2`.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v: [_; 2] = Vec2::new(1., 1.).into();
    /// assert_eq!(v, [1., 1.]);
    /// ```
    #[inline]
    fn into(self) -> [f32; 2] {
        let Vec2 { x, y } = self;
        [x, y]
    }
}

impl Into<(f32, f32)> for Vec2 {
    /// Creates a `[f32; 2]` from a `Vec2`.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v: (_, _) = Vec2::new(1., 1.).into();
    /// assert_eq!(v, (1., 1.));
    /// ```
    #[inline]
    fn into(self) -> (f32, f32) {
        let Vec2 { x, y } = self;
        (x, y)
    }
}

/// Allow `Vec2` to `Debug`.
impl fmt::Debug for Vec2 {
    /// Formats a `Vec2` for debug when `T: Debug`.
    ///
    /// ```
    ///# use math::Vec2;
    /// let v = Vec2::new(1., 1.);
    /// let d = format!("{:?}", v);
    /// assert_eq!(d, "<1.0, 1.0>".to_owned());
    /// ```
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<{:?}, {:?}>", self.x, self.y)
    }
}

use math::Vec3;
use renderer::{RenderCommand, Renderer, Vertex};

#[derive(Clone, Copy)]
pub struct Mesh {
    buf: usize,
    tex: Option<usize>,
}

impl Mesh {
    #[inline]
    const fn new(buf: usize, tex: Option<usize>) -> Self {
        Self { buf, tex }
    }
}

pub struct GlRenderer {
    command_list: Vec<RenderCommand<Mesh>>,
    /// Target shader program. TODO: Move the shader to its own struct.
    shader_program_id: u32,
    /// The ID of the View-Projection uniform in the shader.
    vp_id: i32,
    /// The ID of the Model uniform in the shader.
    m_id: i32,
    /// Collection of VBOs
    vbos: Vec<u32>,
    /// Collection of textures
    texs: Vec<u32>,
}

impl GlRenderer {
    pub fn new(window_width: i32, window_height: i32) -> Self {
        unsafe { gl::Viewport(0, 0, window_width, window_height) };
        unsafe { gl::Enable(gl::CULL_FACE) };
        unsafe { gl::Enable(gl::TEXTURE_2D) };
        unsafe { gl::CullFace(gl::CCW) };

        let shader_program_id = load_shaders_program();
        let vp_id = unsafe {
            let uniform_name = b"vp\0";
            gl::GetUniformLocation(shader_program_id, uniform_name.as_ptr() as _)
        };
        let m_id = unsafe {
            let uniform_name = b"m\0";
            gl::GetUniformLocation(shader_program_id, uniform_name.as_ptr() as _)
        };

        let command_list = Vec::with_capacity(1024);
        let vbos = Vec::with_capacity(32);
        let texs = Vec::with_capacity(32);
        Self { command_list, shader_program_id, vp_id, m_id, vbos, texs }
    }
}

impl Renderer<Mesh> for GlRenderer {
    type Mesh = Mesh;
    type Texture = usize;

    #[inline]
    fn update_viewport(&mut self, width: i32, height: i32) {
        unsafe { gl::Viewport(0, 0, width as _, height as _) };
    }

    #[inline]
    fn frame<F: FnMut(&mut Vec<RenderCommand<Mesh>>)>(&mut self, mut f: F) {
        f(&mut self.command_list);

        unsafe { gl::UseProgram(self.shader_program_id) };

        for command in self.command_list.iter() {
            use RenderCommand::*;

            match command {
                ClearScreen(c) => unsafe {
                    gl::ClearColor(c.x, c.y, c.z, 0.);
                    gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
                }

                SetVPMat(vp) => unsafe {
                    gl::UniformMatrix4fv(self.vp_id, 1, gl::FALSE, (vp) as *const _ as *const _);
                }

                SetModelBasis(b) => unsafe {
                    gl::UniformMatrix4fv(self.m_id, 1, gl::FALSE, (b) as *const _ as *const _);
                }

                DrawMesh(mesh) => unsafe {
                    let vbo = self.vbos[mesh.buf];
                    gl::EnableVertexAttribArray(0);
                    gl::EnableVertexAttribArray(1);
                    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
                    gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, std::mem::size_of::<Vertex>() as _, std::ptr::null());
                    gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE, std::mem::size_of::<Vertex>() as _, std::mem::size_of::<Vec3>() as *const _);
                    if let Some(tex) = mesh.tex {
                        let tex = self.texs[tex];
                        gl::BindTexture(gl::TEXTURE_2D, tex);
                    }

                    gl::DrawArrays(gl::TRIANGLES, 0, 3); // TODO: this is dumb
                    gl::DisableVertexAttribArray(0);
                    gl::DisableVertexAttribArray(1);
                }
            }
        }

        self.command_list.clear();
    }

    #[inline]
    fn load_mesh(&mut self, verts: &[Vertex], tex: Option<Self::Texture>) -> Self::Mesh {
        let mut vbo = 0;
        unsafe {
            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            let mem = std::mem::size_of::<Vertex>() * verts.len();

            gl::BufferData(
                gl::ARRAY_BUFFER,
                mem as _,
                verts.as_ptr() as *const _,
                gl::STATIC_DRAW,
            );
        }

        let buf = self.vbos.len();
        self.vbos.push(vbo);

        Mesh::new(buf, tex)
    }

    #[inline]
    fn load_tex<P: AsRef<std::path::Path>>(&mut self, p: P) -> Option<Self::Texture> {
        let mut texture_id = 0;
        use sdl2::image::LoadSurface;
        let image = sdl2::surface::Surface::from_file(p).unwrap();

        println!("{:?}", image.pixel_format_enum());

        image.with_lock(|bytes| unsafe {
            let mut data = Vec::new();

            for bytes in bytes.chunks(4) {
                match bytes {
                    &[r, b, g, a] => {
                        data.push(r);
                        data.push(b);
                        data.push(g);
                        data.push(a);
                    }

                    _ => unreachable!(),
                }
            }

            gl::GenTextures(1, &mut texture_id);
            gl::BindTexture(gl::TEXTURE_2D, texture_id);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as _);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as _);
            gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as _, image.width() as _, image.height() as _, 0, gl::RGBA, gl::UNSIGNED_BYTE, bytes.as_ptr() as *const _);
            gl::GenerateMipmap(gl::TEXTURE_2D);
        });

        let id = self.texs.len();
        self.texs.push(texture_id);

        Some(id)
    }
}

fn load_shaders_program() -> u32 {
    use std::ptr;

    let vertex_shader_id = unsafe { gl::CreateShader(gl::VERTEX_SHADER) };
    let fragment_shader_id = unsafe { gl::CreateShader(gl::FRAGMENT_SHADER) };

    let vertex_shader_src =
        std::ffi::CString::new(VERTEX_SHADER_SRC).expect("Invalid Vertex Shader");
    let fragment_shader_src =
        std::ffi::CString::new(FRAGMENT_SHADER_SRC).expect("Invalid Fragment Shader");

    let mut log_length = 0;
    unsafe {
        gl::ShaderSource(
            vertex_shader_id,
            1,
            &vertex_shader_src.as_ptr(),
            ptr::null(),
        )
    };
    unsafe { gl::CompileShader(vertex_shader_id) };
    unsafe { gl::GetShaderiv(vertex_shader_id, gl::INFO_LOG_LENGTH, &mut log_length) };

    if log_length != 0 {
        let mut log = vec![0u8; log_length as usize];
        unsafe { gl::GetShaderInfoLog(vertex_shader_id, log_length, std::ptr::null_mut(), log.as_mut_ptr() as _) };
        unsafe { log.set_len(log.len() - 1) };
        let log = std::ffi::CString::new(log).unwrap();
        println!("{:?}", log);
        panic!("Failed to compile the vertex shader.");
    }

    unsafe {
        gl::ShaderSource(fragment_shader_id, 1, &fragment_shader_src.as_ptr(), ptr::null())
    };
    unsafe { gl::CompileShader(fragment_shader_id) };
    unsafe { gl::GetShaderiv(fragment_shader_id, gl::INFO_LOG_LENGTH, &mut log_length) };

    if log_length != 0 {
        let mut log = vec![0u8; log_length as usize];
        unsafe { gl::GetShaderInfoLog(fragment_shader_id, log_length, std::ptr::null_mut(), log.as_mut_ptr() as _) };
        unsafe { log.set_len(log.len() - 1) };
        let log = std::ffi::CString::new(log).unwrap();
        println!("{:?}", log);
        panic!("Failed to compile the fragment shader.");
    }

    // TODO: Get error results from the shader compilation step using glGetShaderiv to
    // read in GL_COMPILE_STATUS and GL_INFO_LOG_LENGTH.

    let program_id = unsafe { gl::CreateProgram() };
    unsafe { gl::AttachShader(program_id, vertex_shader_id) };
    unsafe { gl::AttachShader(program_id, fragment_shader_id) };
    unsafe { gl::LinkProgram(program_id) };

    // TODO: Same as above, check the link step using glGetProgramiv.

    unsafe { gl::DetachShader(program_id, vertex_shader_id) };
    unsafe { gl::DetachShader(program_id, fragment_shader_id) };

    unsafe { gl::DeleteShader(vertex_shader_id) };
    unsafe { gl::DeleteShader(fragment_shader_id) };

    program_id
}

const VERTEX_SHADER_SRC: &str = r#"
    #version 130

    in vec3 position;
    in vec2 texcoord;

    uniform mat4 vp;
    uniform mat4 m;

    out vec4 pixel_color;
    out vec2 tex_coord;

    void main() {
        // We are passed in a row-major vector, so we need to do vec * mat.
        gl_Position = vec4(position, 1) * m * vp;
        tex_coord = texcoord;

        pixel_color = vec4(1, 1, 1, 1);
    }
"#;

const FRAGMENT_SHADER_SRC: &str = r#"
    #version 130

    in vec4 pixel_color;
    in vec2 tex_coord;

    uniform sampler2D target_tex;

    out vec4 color;

    void main() {
        color = texture(target_tex, tex_coord);
    }
"#;

#![windows_subsystem = "windows"]

mod opengl;

use math::{CrossProduct, Mat4, Vec3};
use crate::opengl::GlRenderer;
use renderer::{CommandList, Renderer, Vertex};

extern crate gl;
extern crate sdl2;

use sdl2::{event::Event, keyboard::Keycode};

struct Camera {
    pos: Vec3,
    dir: Vec3,
    right: Vec3,
    up: Vec3,
    aspect_ratio: f32,
    fov: f32,
}

impl Camera {
    #[inline]
    pub fn new(pos: Vec3) -> Self {
        let dir = Vec3::Z;
        let right = Vec3::Y.cross(dir).normalize();
        let up = dir.cross(right).normalize();
        let aspect_ratio = 800. / 600.;
        let fov = std::f32::consts::FRAC_PI_4;

        Self { pos, dir, right, up, aspect_ratio, fov }
    }

    pub fn rotate_z(&mut self, rad: f32) {
        let m = Mat4::y_rotation(rad);
        self.dir = (m * self.dir.to_vec4()).into();
        self.right = self.up.cross(self.dir);
    }

    #[inline]
    pub fn view(&self) -> Mat4 {
        let pos = self.pos;
        let dir = self.dir;
        let right = self.right;
        let up = self.up;

        let a: Mat4 = [
            right.x, right.y, right.z, 0.,
            up.x, up.y, up.z, 0.,
            dir.x, dir.y, dir.z, 0.,
            0., 0., 0., 1.,
        ].into();

        let b: Mat4 = [
            1., 0., 0., -pos.x,
            0., 1., 0., -pos.y,
            0., 0., 1., -pos.z,
            0., 0., 0., 1.,
        ].into();

        a * b
    }

    #[inline]
    pub fn proj(&self) -> Mat4 {
        let scale = 1. / self.fov.tan();
        let far = 1000.;
        let near = 0.1;

        let r = self.aspect_ratio * scale;
        let l = -r;
        let t = scale;
        let b = -t;

        [ 2. * near / (r - l), 0., (r + l) / (r - l), 0.,
          0., 2. * near / (t - b), (t + b) / (t - b), 0.,
          0., 0., -(far + near) / (far - near), -2. * far * near / (far - near),
          0., 0., -1., 0., ].into()
    }

    #[inline]
    pub fn view_proj(&self) -> Mat4 {
        self.proj() * self.view()
    }
}

fn main() -> Result<(), String> {
    let context = sdl2::init()?;
    let video_subsystem = context.video()?;
    let time_subsystem = context.timer()?;
    let _image_subsystem = sdl2::image::init(sdl2::image::InitFlag::all())?;

    let mut canvas = video_subsystem
        .window("SDL2 Demo", 1600, 1200)
        .opengl()
        .position_centered()
        .resizable()
        .build()
        .map_err(|e| e.to_string())?
        .into_canvas()
        .build()
        .map_err(|e| e.to_string())?;

    // Setting the OpenGL GetProcAddress to load functions
    gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const _);
    canvas.window().gl_set_context_to_current().map_err(|e| e.to_string())?;

    let mut event_pump = context.event_pump()?;

    let mut t = 0.0f64;
    let dt = 0.01f64;

    let get_time = || {
        (time_subsystem.performance_counter() as f64)
            / (time_subsystem.performance_frequency() as f64)
    };

    let mut current_time = get_time();
    let mut accumulator = 0.0f64;

    let mut camera = Camera::new(Vec3::new(0., 3., 3.));
    let mut renderer = GlRenderer::new(800, 600);

    let tex = renderer.load_tex("assets/image.png").unwrap();
    let mesh = {
        let triangle = vec![
            Vertex::new(Vec3::new(-1., -1., 0.), [0., 0.,].into()),
            Vertex::new(Vec3::new( 1., -1., 0.), [1., 0.,].into()),
            Vertex::new(Vec3::new( 0.,  1., 0.), [0.5, 1.,].into()),
        ];
        renderer.load_mesh(&triangle, Some(tex))
    };

    'running: loop {
        let mut move_forward = false;
        let mut move_back = false;
        let mut yaw_left = false;
        let mut yaw_right = false;

        for event in event_pump.poll_iter() {
            use sdl2::event::WindowEvent;

            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                }

                | Event::KeyDown {
                    keycode: Some(Keycode::W),
                    ..
                } => {
                    move_forward = true;
                }

                | Event::KeyDown {
                    keycode: Some(Keycode::S),
                    ..
                } => move_back = true,

                | Event::KeyDown {
                    keycode: Some(Keycode::A),
                    ..
                } => yaw_left = true,

                | Event::KeyDown {
                    keycode: Some(Keycode::D),
                    ..
                } => yaw_right = true,

                Event::MouseMotion { .. } => (),

                Event::Window {
                    win_event: WindowEvent::Resized(width, height),
                    ..
                } => {
                    camera.aspect_ratio = (width as f32) / (height as f32);
                    renderer.update_viewport(width, height);
                }

                e => {
                    println!("{:?}", e);
                }
            }
        }

        renderer.frame(|command_list| {
            let new_time = get_time();
            let frame_time = {
                let diff = new_time - current_time;
                if diff > 0.25 {
                    0.25
                } else {
                    diff
                }
            };

            current_time = new_time;
            accumulator += frame_time;

            if move_forward {
                camera.pos -= Vec3::Z * 5. * (dt as f32);
            }

            if move_back {
                camera.pos += Vec3::Z * 5. * (dt as f32);
            }

            while accumulator >= dt {
                // previous_state = current_state;
                // integrate(current_state, t, dt);
                accumulator -= dt;
                t += dt;

                if yaw_right {
                    camera.rotate_z(3.14159 * -0.5 * (dt as f32));
                }

                if yaw_left {
                    camera.rotate_z(3.14159 * 0.5 * (dt as f32));
                }
            }

            // alpha = accumulator / dt;
            // state = current_state * alpha + previous_state * (1. - alpha);
            // render(state);

            command_list.push_clearscreen(Vec3::new(0.6, 0., 0.8));

            let matrix = camera.view_proj();
            command_list.push_vp_matrix(matrix);
            command_list.push_model_basis(Mat4::scale([3.0, 3.0, 3.0,].into()));
            command_list.push_draw_mesh(mesh);

            // TODO: Move `canvas` to the renderer.
            canvas.present();
        });
    }

    Ok(())
}
